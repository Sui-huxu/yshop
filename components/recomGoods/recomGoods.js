// components/recomGoods/recomGoods.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    comList:{
        type:Array,
        value:[]
    },
    obj:{
        type:Number,
        value:0
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    // 跳转详情页
    prodetail(e){
      let id = e.currentTarget.dataset.id
      console.log(id);
      wx.navigateTo({
        url: '/pages/productDetails/productDetails?id='+id,
      })
    },

  }
})
