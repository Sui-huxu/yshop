// 引入env中的url
const {
  baseUrl
} = require('./env.js').dev;
//在这里添加我们的专业域名
const subDomain = 'api/';

var header = {
  "content-type": "application/json;charset=UTF-8", // 请求数据类型根据需求自行更改
};
module.exports = {
  /**
   * 二次封装wx.request
   * url:请求的接口地址
   * method:请求方式 GET,POST....
   * data:要传递的参数
   *isSubDomain:表示是否添加二级子域名 true代表添加, false代表不添加
   */
  request: (url, method, data, isSubDomain = true) => {
    //这里使用ES6的写法拼接的字符串

    let _url = `${baseUrl}${isSubDomain ? subDomain: '' }${url}`;
    return new Promise((resolve, reject) => {
      header["Authorization"] = "Bearer " + wx.getStorageSync('token')
      wx.showLoading({
        title: '正在加载',
      });
      wx.request({
        url: _url,
        data: data,
        method: method,
        header: header,
        success: (res) => {
          console.log('从接口获取到的数据：', res.data);
          let {
            data
          } = res;
          if (data.status == '200') {
            resolve(data);
            wx.hideLoading();
          } else {
            reject(data);
            wx.hideLoading();
          }
        },
        fail: (err) => {
          wx.hideLoading();
        }
      });
    });
  },
}