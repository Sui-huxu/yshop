//引入封装的reuest请求
const {
  request
} = require('./request.js')
//基于业务封装的接口
module.exports = {
  // 登录/注册验证码接口
  registerApi: (data) => {
    return request('register/verify', 'POST', data)
  },
  // 注册完成
  registerresApi: (data) => {
    return request('register', 'POST', data)
  },
  // 手机登录
  mobileApi: (data) => {
    return request('login/mobile', 'POST', data)
  },
  // 登录接口
  loginApi: (data) => {
    return request('login', 'GET', data)
  },
  // 推荐商品接口
  hotApi: (data) => {
    return request('product/hot', 'GET', data)
  },
  // 全部商品的接口
  productsApi: (data) => {
    return request('products', 'GET', data)
  },
  // 积分商品的接口
  productsIntegralApi: (data) => {
    return request('products/integral', 'GET', data)
  },
  // 首页接口
  indexApi: (data) => {
    return request('index', 'GET', data)
  },
  // 搜索接口
  searchApi: (data) => {
    return request('search/keyword', 'GET', data)
  },
  //首页头部接口
  headerApi: (data) => {
    return request('getCanvas', 'GET', data)
  },

  // 图文资讯接口
  articleApi: (data) => {
    return request('article/list', 'GET', data)
  },
  // 图文资讯详情接口
  detailsApi: (data) => {
    return request('article/details', 'GET', data)
  },
  // 收藏接口
  collectApi: (data) => {
    return request('collect/user', 'GET', data)
  },
  // 删除收藏接口
  delApi: (data) => {
    return request('collect/del', 'POST', data)
  },
  // 优惠券接口
  couponsApi: (data) => {
    return request('coupons', 'GET', data)
  },
  // 领取优惠券
  receiveApi: (data) => {
    return request('coupon/receive', 'POST', data)
  },
  // 拼团接口
  GroupApi: (data) => {
    return request('combination/list', 'GET', data)
  },
  // 拼团详情接口
  GroupdetailsApi: (data, goodID) => {
    return request('combination/detail' + goodID, 'GET', data)
  },
  // 积分接口
  integralApi: (data) => {
    return request('sign/user', 'POST', data)
  },
  integralconfigApi: (data) => {
    return request('sign/config', 'GET', data)
  },
  // 积分列表
  integrallistApi: (data) => {
    return request('sign/list', 'GET', data)
  },
  // 签到接口
  SigninApi: (data) => {
    return request('sign/integral', 'POST', data)
  },
  // 明细接口
  monthApi: (data) => {
    return request('sign/month', 'GET', data)
  },
  // 秒杀专区
  seckillApi: (data) => {
    return request('seckill/index', 'GET', data)
  },
  // 秒杀专区商品
  seckillListApi: (data, goodID) => {
    return request('seckill/list' + goodID, 'GET', data)
  },
  // 分类接口
  listApi: (data) => {
    return request('category', 'GET', data)
  },
  // 砍价专区
  bargainApi: (data) => {
    return request('bargain/list', 'GET', data)
  },
  //商品详情页接口
  productApi: (data, id) => {
    return request('product/detail' + id, 'GET', data)
  },
  // 评价接口
  replyApi: (data, id) => {
    return request('reply/config' + id, 'GET', data)
  },
  // 评价列表接口
  replyListApi: (data, id) => {
    return request('/reply/list' + id, 'GET', data)
  },
  // 购物车列表接口
  cartlistApi: (data) => {
    return request('cart/list', 'GET', data)
  },
  // 购物车数量接口
  cartcountApi: (data) => {
    return request('cart/count', 'GET', data)
  },

  // 我的页面接口
  userApi: (data) => {
    return request('menu/user', 'GET', data)
  },
  // 用户信息
  infoApi: (data) => {
    return request('userinfo', 'GET', data)
  },
  //余额接口
  balanceApi: (data) => {
    return request('user/balance', 'GET', data)
  },
  //我的页面优惠券接口
  couponsUserApi: (data) => {
    return request('coupons/user/0', 'GET', data)
  },
  //会员中心接口
  gradeApi: (data) => {
    return request('user/level/grade', 'GET', data)
  },
  //升级任务接口
  taskApi: (data) => {
    return request('user/level/task', 'GET', data)
  },
  //门店列表接口
  storlistApi: (data) => {
    return request('store_list', 'GET', data)
  },
  //上传头像
  uploadApi: (data) => {
    return request('api/upload', 'POST', data)
  },
  // 秒杀专区商品详情
  seckillDetailApi: (data, goodID) => {
    return request('seckill/detail' + goodID, 'GET', data)
  },
  //更改购物车商品数量
  cartNumApi: (data) => {
    return request('cart/num', 'POST', data)
  },
  //购物车删除
  cartDelApi: (data) => {
    return request('cart/del', 'POST', data)
  },
  //更改头像
  editApi: (data) => {
    return request('user/edit', 'POST', data)
  },
  // 退出登录接口
  logoutApi: (data) => {
    return request('auth/logout', 'POST', data)
  },
  //全部订单
  orderListApi: (data) => {
    return request('order/list', 'GET', data)
  },
  orderDataApi: (data) => {
    return request('order/data', 'GET', data)
  },
  // 地址列表
  addressListApi: (data) => {
    return request('address/list', 'GET', data)
  },
  // 默认地址
  addressDefaultApi: (data) => {
    return request('address/default/set', 'POST', data)
  },
  // 删除收货地址
  addressDelApi: (data) => {
    return request('address/del', 'POST', data)
  },
  // 编辑、新增收货地址
  addressEditApi: (data) => {
    return request('address/edit', 'POST', data)
  },
  // 城市列表
  cityListApi: (data) => {
    return request('api/city_list', 'GET', data)
  },
  // 城市列表
  registerResetApi: (data) => {
    return request('register/reset', 'POST', data)
  },
  // 砍价列表
  barginDetailApi: (data) => {
    return request('bargain/detail' + data, 'GET', )
  },
  // 砍价详情
  barginHelpApi: (data) => {
    return request('bargain/help/count', 'POST', data)
  },
  // 加入购物车接口
  cartaddApi: (data) => {
    return request('cart/add', 'POST', data)
  },
  // 收藏接口
  collectaddApi: (data) => {
    return request('collect/add', 'POST', data)
  },
  // 购物车添加页面接口
  getcartadd: (data) => {
    return request('cart/add', 'POST', data)
  },
  // 订单页面积分接口
  integral: (data) => {
    return request('userinfo', 'GET', data)
  },
  // 订单页面接口
  getbalance: (data) => {
    return request('order/confirm', 'POST', data)
  },
  // 新添地址接口
  city_listApi: (data) => {
    return request('city_list', 'GET', data)
  },
  // 完成支付页面接口
  orderPaymentApi: (data) => {
    return request(`order/detail/${data}`, 'Get', )
  },
  // 完成支付页面接口
  orderPaymentsApi: (data, url) => {
    return request(`order/create` + url, 'POST', data)
  },
  // 订单页面积分接口
  orderCoomputed: (data, url) => {
    return request('order/computed' + url, 'POST', data)
  },
  // 订单页面优惠劵接口
  couponsOrder: (url) => {
    return request('coupons/order' + url, 'GET')
  },
  //砍价列表
  bargainlistApi: (data) => {
    return request('bargain/user/list', 'GET')
  },
  // 订单支付接口
  orderPayApi: (data) => {
    return request('order/pay', 'POST', data)
  },
  // 售后/退款
  storeAfterSalesListApi: (data) => {
    return request('storeAfterSales/list', 'GET', data)
  },
  // 售后详情
  storeDetailApi: (id, key) => {
    return request(`store/detail/${key}/${id}`, 'GET')
  },
  // 撤销申请
  revokeApi: (id, key) => {
    return request(`revoke/${key}/${id}`, 'GET')
  },
  //取消订单
  orderCancelApi: (data) => {
    return request(`order/cancel`, 'POST', data)
  },
  //确认收货
  orderTakeApi: (data) => {
    return request(`order/take`, 'POST', data)
  },
  //密码登陆
  pwdLoginApi: (data) => {
    return request('login', 'POST', data)
  },
  // 订单信息
  unpayApi: (id) => {
    return request('order/detail/' + id, 'GET', {}, )
  },
  // 取消订单
  cancelorderApi: (data) => {
    return request('order/cancel', 'POST', data, )
  },
  // 积分签到页面的头部信息
  integralInfo: () => {
    return request('userinfo?login=true')
  },
  // 微信支付
  wechatpayApi: (data) => {
    return request('order/pay', 'POST', data, )
  },
  // 删除商品
  delCartApi: (data) => {
    return request('cart/del', 'POST', data)
  },
  // 评价
  orderProductApi: (data) => {
    return request('order/product', 'POST', data)
  },
  // 评价去评价
  orderCommentApi: (data) => {
    return request(`order/comment`, 'POST', data)
  },
}