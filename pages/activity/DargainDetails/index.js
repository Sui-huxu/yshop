// pages/activity/DargainDetails/index.js
import {barginDetailApi,barginHelpApi} from "../../../http/api"
Page({
  /**
   * 页面的初始数据
   */
  data: {
    goods:{},//商品信息 
    dds:'',//倒计时
    time:'',//页面倒计时
    titletime:'',//定时器
    bargintrue:true, //是否开始砍价
    show:false, //是否显示蒙版
    id:'0',//砍价id
    productId:'0',//商品id
    width:'',//进度条
    userBargainStatus:true,//
    obj:true,//点击加载更多
    userInfo:[],//砍价用户
    alreadyPrice:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  // 结束时间转化
  formatDate(stopTime) {
    let _self=this;
    // 方式1 转换为'yyyy-MM-dd HH:mm:ss'
    function add0(num) { return num < 10 ? '0' + num : num } // 个位数的值在前面补0
    const date = new Date(stopTime)
    const Y = date.getFullYear()
    const M = date.getMonth() + 1
    const D = date.getDate()
    const h = date.getHours()
    const m = date.getMinutes()
    const s = date.getSeconds()
    const dateString = Y + '-' + add0(M) + '-' + add0(D) + '  ' + add0(h) + ':' + add0(m) + ':' + add0(s)
    let dateStrings =Math.round(new Date(dateString))/1000;
    var nowTime = +new Date()/1000;
    let dds = _self.timers(dateStrings);
    return dds
  },
  onLoad(options) {
    let _self =this;
    let id ='/'+ options.id;
    console.log(id);
    barginDetailApi(id).then(res =>{
      console.log(res);
      let goods =res.data.bargain;
      let {productId} = goods;
      let {id} =goods;
      let userInfo = res.data.userInfo
      console.log(userInfo);
      let stopTime =goods.stopTime;
      // 砍价状态接口
      barginHelpApi({
        bargainId: res.data.bargain.id,
        bargainUserUid:res.data.userInfo.uid
      }).then(resn=>{
        console.log(resn);
        let alreadyPrice = resn.data.alreadyPrice
        let width = resn.data.pricePercent +'%'
        let userBargainStatus = resn.data.userBargainStatus
        _self.setData({
          width,
          userBargainStatus,
          alreadyPrice
        })
      })
      let titletime = setInterval(function(){
        // 倒计时时间显示
        let time=_self.formatDate(stopTime)
        _self.setData({
          time,
          titletime
        })
      },500)  
      _self.setData({
        goods,
        userInfo,
        bargintrue:_self.data.bargintrue,
        id,
        productId
      }) 
    })
  },

  // 倒计时
  timers(time){
    var nowTime = +new Date()/1000;//返回当前时间的总的毫秒数
    var times = ( time - nowTime )
    var d = parseInt(times / 60 / 60 / 24); //天
    var h = parseInt(times / 60 / 60 % 24); //时
    h = h < 10 ? '0' + h : h;
    var m = parseInt(times / 60 % 60); //分
    m = m < 10 ? '0' + m : m;
    var s = parseInt(times % 60); //当前的秒
    s = s < 10 ? '0' + s : s;
    let dds =  d + '天' + h + '时' + m + '分' + s + '秒'; 
    return dds;
  },
  // 立即开始砍价
  startbtn(){
    let _self =this;
    let bargintrue=false;
    let show =true
    _self.setData({
      bargintrue,
      show
    })
  },
  // 隐藏蒙版
  hidemask(){
    let _self =this;
    let show = false;
    let bargintrue =false
    _self.setData({
      show,
      bargintrue
    })
  },
  // 去订单页
  toOrder(){
    let _self =this;
    let productId =_self.data.productId;
    let id =Number(_self.data.id);;
    console.log(productId,id);
    wx.navigateTo({
      url: '/pages/orderDetails/orderDetails?productId='+productId+'&id='+id,
    })
  },
  //跳转砍价页面
  GoodsBargain(){
    wx.navigateTo({
      url:'../GoodsBargain/index'
    })
  },
  more(){
    let _self = this
    let obj = !_self.data.obj
    _self.setData({
      obj
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    let _self =this
    let titletime =_self.data.titletime
    clearInterval(titletime)
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})