// pages/activity/GroupDetails/index.js
import {
  GroupdetailsApi,
  seckillDetailApi,
  getcartadd,
  productApi
} from '../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    date: {}, //商品详情
    storeInfo: {}, //商品头部详情
    productValue: {}, //选择商品条件
    reply: {},
    isShow: 'hidden',
    url: "",
    dds: '00时00分00秒',
    i: 0,
    chooseData: [],
    choseFlag: false,
    arres:'',
    goodsParametric:{},
    num:1,
    obj:false,
    duy:false,
    pink:{},//拼单人员
    pinkStock:0,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  // 循环判断商品详情
  goodsFor(){
    let _self = this
    let chooseData = _self.data.date.productAttr
    console.log(chooseData);
    let arr = []
     for ( let i = 0 ; i<chooseData.length;i++) {
      var  namdl = chooseData[i]
      for(let j = 0 ; j<namdl.attrValue.length;j++){
        var obj = namdl.attrValue[j].check
        if(obj){
          arr.push( namdl.attrValue[j].attr)
          if(arr.length == 2){
            if(arr[0] == '90m'){
              arr.push(arr[0])
              arr.shift()
            }
          }
        }
      }
    }
    setTimeout(()=>{
      let arres = String(arr)
      console.log(arres);
      let goodsParametric = _self.data.productValue[`${arres}`]; 
      console.log(goodsParametric);
      let pinkStock = goodsParametric.pinkStock
      _self.setData({
        arres,
        pinkStock,
        goodsParametric,
      })
    },100)
  },

  // 商品数量判断
  initial(){
    setTimeout(()=>{
      let _self= this
      let goodsNum = _self.data.goodsParametric.pinkStock;
      let mixNum = _self.data.num
      console.log(mixNum,goodsNum);
      if(goodsNum == 0){
        _self.setData({
          num:0
        })
      }else{
        _self.setData({
          num:1
        })
      }
    },200)
  },
  onLoad(options) {
    let _self = this
    console.log(options);
    let id = '/' + options.id
    console.log(id);
    _self.setData({
      id
    })
    let url = options.url
    if (url == '拼团') {
      seckillDetailApi(_self.data.list, id).then(res => {
        console.log(res);
        console.log(res.data.productAttr);
        let date = res.data
        let storeInfo = date.storeInfo
        let productValue = date.productValue
        let chooseData = date.productAttr
        let reply = date.reply
        let stop = options.stop
        chooseData.map(item=>{
          item.attrValue[0].check = true
        })
        setInterval(() => {
          let nowTime = +new Date() / 1000; //返回当前时间的总的毫秒数
          let inputTime = Number(stop)
          let times = (inputTime - nowTime)
          let h = parseInt(times / 60 / 60 % 24); //时
          h = h < 10 ? '0' + h : h;
          let m = parseInt(times / 60 % 60); //分
          m = m < 10 ? '0' + m : m;
          let s = parseInt(times % 60); //当前的秒
          s = s < 10 ? '0' + s : s;
          let dds = h + '时' + m + '分' + s + '秒';
          _self.setData({
            dds
          })
        }, 500)
        _self.setData({
          url,
          date,
          storeInfo,
          productValue,
          reply,
          chooseData,
        })
        _self.goodsFor()
      })
    } else {
      GroupdetailsApi(this.data.list, id).then(res => {
        console.log(res);
        let _self = this
        let date = res.data
        let storeInfo = date.storeInfo
        let productValue = date.productValue
        let chooseData = date.productAttr
        let reply = date.reply
        let pink = date.pink
        chooseData.map(item=>{
          item.attrValue[0].check = true
        })
        _self.setData({
          date,
          storeInfo,
          productValue,
          reply,
          chooseData,
          pink
        })
        _self.goodsFor()
        _self.initial()
      })
    }
  },
  // 收藏
  imgIf(){
    let _self = this
    let obj = !_self.data.obj
    _self.setData({
      obj
    })
  },
  //关闭弹窗
   overflow() {
    let _self = this;
    let duy = !_self.data.duy
    _self.animation.translateY(240).step();
    _self.data.isShow = "hidden";
    _self.setData({
      // export导出动画
      animationData: _self.animation.export(),
      isShow: _self.data.isShow,
      duy
    })
    _self.goodsFor()
  },
  //开启弹窗
  pop_up() {
    let _self = this;
    let duy = !_self.data.duy
    _self.data.isShow = "show";
    // 创建动画
    let animation = wx.createAnimation({
      // 时间
      duration: 200,
      // 动画效果
      timingFunction: 'ease-in-out',
    })
    // 给小程序对象添加一个动画属性=创建动画的实例
    _self.animation = animation;
    // 动画怎么动（浅拷贝）
    // y轴上为负的，y轴下为负 
    // step停止
    animation.translateY(-800).step();
    _self.setData({
      // export导出动画
      animationData: _self.animation.export(),
      isShow: _self.data.isShow,
    })
    let chooseData = this.data.date.productAttr
    if (!this.data.choseFlag) {
      console.log(chooseData);
      chooseData.map(item => {
        item.attrValue[0].check = true
      })
    }
    if(_self.data.duy){
      let cartNum = _self.data.num
      let combinationId = Number(_self.data.id.slice(1))
      let price = _self.data.date.storeInfo.price
      let productId = _self.data.date.storeInfo.productId
      let uniqueId = _self.data.goodsParametric.unique
      console.log(cartNum,combinationId,price,productId,uniqueId);
      if(cartNum>0){
        getcartadd({
          cartNum,
          combinationId,
          new:1,
          price,
          productId,
          uniqueId
        }).then(res=>{
          console.log(res);
          let cartId = res.data.cartId
          if(res.status == 200){
            _self.overflow()
            let duy = !_self.data.duy
            _self.setData({duy})
            wx.navigateTo({
              url: '../../productDetails/payment/payment?cartId='+cartId
            })
          }else{
            wx.showToast({
              title: res.msg,
              icon: 'error'
            })
          }
        })
      }else if(cartNum == 0 ){
        wx.showToast({
          title: '库存为零',
          icon: 'error'
        })
      }
    }
    this.setData({
      chooseData,
      duy
    })

  },
  // 跳转独自购买
  alone(e) {
    console.log(e);
    let {id} = e.currentTarget.dataset
    console.log(id);
    productApi({latitude: '',longitude:''},'/'+id).then(res=>{
      console.log(res);
      if(res.status == 200){
        wx.navigateTo({
          url: '../../../pages/productDetails/productDetails?id=' + id,
        })
      }else{
        wx.showToast({
          title: res.msg,
          icon: 'error'
        })

      }

    })


  },
  //选择商品
  backred(e) {
    this.setData({
      choseFlag: true
    })
    let _self = this
    console.log(e);
    let i = e.currentTarget.dataset.a
    let b = e.currentTarget.dataset.b
    console.log(i, b);
    _self.data.chooseData.map(item => {
      if (item.attrName == b) {
        let iten = item
        console.log(iten);
        item.attrValue[0].check = !item.attrValue[0].check
        item.attrValue[1].check = !item.attrValue[1].check
      }
    })
    _self.goodsFor()
    _self.setData({
      chooseData: _self.data.chooseData
    })
    _self.initial()
  },
  
  // 商品减
  removeGoods() {
    let _self = this
    if (_self.data.num <= 1) {
      return wx.showToast({
        title: '不能再减啦！',
        icon: 'error'
      })
    } else {
      _self.setData({
        num:--_self.data.num
      })
    }
  },
  // 商品加
  addGoods() {
    let _self = this
    let goodsNum = _self.data.goodsParametric.pinkStock;
    if(_self.data.num >= goodsNum){
      return wx.showToast({
        title: '超过商品库存!',
        icon: 'error'
      })
    } else {
      _self.setData({
        num:++_self.data.num
      })
    }
  },
  //输入框数量判断 
  numberJudge(e){
    let _self = this
    let inputNum = e.detail.value
    let goodsNum = _self.data.goodsParametric.pinkStock;
    if(inputNum > goodsNum) { 
        wx.showToast({
        title: '超过商品库存',
        icon: 'error'
      })
      _self.setData({
        num:goodsNum
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})