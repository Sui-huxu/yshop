// pages/activity/GoodsGroup/index.js
import {GroupApi } from '../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    number: {
      page: 1,
      limit: 10,
    }, //接口传参
    list:[],//拼团数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    GroupApi(this.data.number).then(res=>{
      var list = res.data.storeCombinationQueryVos
      console.log(list);
      this.setData({
        list
      })
    })
  },
  // 跳转拼团商品
  goodsDetails(e){
    var id = e.currentTarget.dataset.id
    console.log(id);
    wx.navigateTo({
      url:'/pages/activity/GroupDetails/index?id='+id,
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})