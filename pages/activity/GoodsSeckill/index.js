// pages/activity/GoodsSeckill/index.js
import { seckillApi,seckillListApi } from '../../../http/api'

Page({

  /**
   * 页面的初始数据信息
   */
  data: {
    goods:{},
    string:'',//商品状态
    dds:'',//倒计时
    numbar:{
      page: 1,
      limit: 5
    },//调取商品
    date:{},//时间数据
    num:0,//点击变换
    goodsList:[],//商品数据
    sdfsdf:{},
    obj:false,
    status:0,//判断商品是否还能买
    titletime:'',
    seten:true
  },
  /**
   * 生命周期函数--监听页面加载
   */
  //获取时间封装
  timers(goods){
    let nowTime = +new Date()/1000;//返回当前时间的总的毫秒数
    let inputTime = goods.stop //传过来的时间
    let times = ( inputTime - nowTime )
    let d = parseInt(times / 60 / 60 / 24); //天
    d = d < 10 ? '0' + d : d;
    let h = parseInt(times / 60 / 60 % 24); //时
    h = h < 10 ? '0' + h : h;
    let m = parseInt(times / 60 % 60); //分
    m = m < 10 ? '0' + m : m;
    let s = parseInt(times % 60); //当前的秒
    s = s < 10 ? '0' + s : s;
    let dds =  d + '天' + h + '时' + m + '分' + s + '秒'; 
    return dds
  },
  //定时器封装
  opentime(goods){
    let _self = this
    let titletime = setInterval(()=>{
      // 关闭定时器
      console.log(_self.data.obj);
      if(_self.data.obj){
        clearInterval(titletime);
      }
      let dds = _self.timers(goods)
      _self.setData({
        dds,
        titletime
      })
    },500)
  },
  //获取商品列表
  goodslist(num){
    let  _self = this
      seckillListApi(_self.data.numbar,'/'+num).then(res=>{
        console.log(res);
      let goodsList = res.data
      _self.setData({
        goodsList
      })
      console.log(_self.data.goodsList);
    })
  },
  // 定时器
  timerr(){
    seckillApi().then(res=>{
      let  _self = this
      let date = res.data.seckillTime
      for(let i = 0 ; i < 7; i++){
        let list = date[i]
        let numav = list.state
        if(numav == '抢购中') {
          let seten = true
          let goods = date[i]
          let num = goods.id
          let status = goods.status
          _self.opentime(goods)
          _self.goodslist(num)
          _self.setData({
            goods,
            status,
            num,
            date,
            seten
          })
          return
        }
      }
    })
  },

  onLoad(options) {  
    let _self = this
    _self.timerr()
  },
  // 点击事件
  click(e){
    let  _self = this
    let titletime = _self.data.titletime
    clearInterval(titletime)
    let {id} = e.currentTarget.dataset;
    console.log(id);
    console.log(e.currentTarget);
    let {status} = e.currentTarget.dataset;
    console.log(status);
    let {date} = e.currentTarget.dataset; // 返回来的时间
    console.log(date);
    let nowTime = +new Date()/1000;//返回当前时间的总的毫秒数
    _self.setData({
      status,
      num:id,
    })
    console.log(nowTime>date);
    if(nowTime>date){
      let redu = '活动已经结束'
      let obj = true
      _self.setData({
        obj,
        dds:redu,
      })
    }else{
    seckillApi().then(res=>{
      let  _self = this
      let obj = false
      let date = res.data.seckillTime
      for(let i = 0 ; i < 7; i++){
        let list = date[i]
        let numav = list.id
        console.log(numav);
        if(numav == id) {
          let seten = false
          let goods = date[i]
          let num = goods.id
          let dds = _self.opentime(goods)
          _self.goodslist(num)
          _self.setData({
              obj,
              num,
              date,
              dds,
              seten
          })
          return
        }
      }
    })
    }
    let num = _self.data.num;
    _self. goodslist(num);
  },
  // 跳转拼团商品
  goodsDetails(e){
    let status = e.currentTarget.dataset.status
    console.log(status);
    let id = e.currentTarget.dataset.id
    console.log(id);
    let stop = e.currentTarget.dataset.stop
    console.log(stop);
  if(status == 1){
    wx.navigateTo({
      url: '../GroupDetails/index?id='+id+'&url='+'拼团'+'&stop='+stop,
    });
  }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {
    let _self = this
    let obj = true
    _self.setData({
      obj
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})