// pages/activity/GoodsBargain/index.js
import { bargainApi } from '../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    numbar:{
      page: 1,
      limit: 20
    },//砍价传参
    list:{},//砍价详情
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    bargainApi().then(res =>{
      let _seif = this
      console.log(res);
      var list = res.data
      _seif.setData({
        list
      })
    })

  },

  //跳转砍价
  bargain(e){
    let id = e.currentTarget.dataset.id
    console.log(id);
    wx.navigateTo({
      url: '../DargainDetails/index?id='+id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})