// pages/more/address/edieAddress/editAddress.js
import {addressEditApi,city_listApi} from '../../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    realName:'',
    phone:'',
    detail:'',
    isDefault:false,
    isShow: 'hide',
    data:[],
    province:'请选择',//省
    province1:'',//市
    province2:'',//区
    pid:'',
    region:'请选择',
    city_id:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(JSON.parse(options.item));
    if (options) {
      let infoList = JSON.parse(options.item)
      let province = infoList.province
      let province1 = infoList.city
      let province2 = infoList.district
      let region = province+province1+province2
      let realName = infoList.realName
      let phone = infoList.phone
      let detail = infoList.detail
      let isDefault = infoList.isDefault
      console.log(infoList);
      this.setData({
        region,
        realName,
        phone,
        detail,
        isDefault
      })
    }

  },
  saveAdress(){
    let _self = this
    let address = {
      province:_self.data.province,
      district:_self.data.province2,
      city:_self.data.province1,
      city_id:_self.data.city_id
    }
    console.log(address);
    let real_name = _self.data.realName
    let phone = _self.data.phone
    let detail = _self.data.detail
    let is_default = _self.data.isDefault

    let post_code=""
    let params = {address,detail,is_default,phone,post_code,real_name,}
    addressEditApi(params).then(res=>{
      console.log(res);
    })
    wx.navigateBack()
  },
  address(){
    let _self = this;
    _self.data.isShow = "show";
    // 创建动画
    let animation = wx.createAnimation({
      // 时间
      duration: 200,
      // 动画效果
      timingFunction: 'ease-in-out',
    })
    console.log(_self);
    // 给小程序对象添加一个动画属性=创建动画的实例
    _self.animation = animation;
    // 动画怎么动（浅拷贝）
    // y轴上为负的，y轴下为负 
    // step停止
    animation.translateY(-460).step();
    _self.setData({
      isShow: _self.data.isShow,
      // export导出动画
      animationData: _self.animation.export()
    })
    city_listApi().then(res=>{
      console.log(res);
      let data = res.data
      let province = '请选择'
      let province1 = ''
      let province2 = ''
      _self.setData({
        data,
        province,
        province1,
        province2
      })
    })
  },
  popUp(){
    let _self = this;
    _self.animation.translateY(460).step();
    _self.data.isShow = "hide";
    _self.setData({
      // export导出动画
      animationData: _self.animation.export(),
      isShow: _self.data.isShow,
    })
  },
  // 选择城市
  chosecity(e){
    let _self = this
    let index = e.currentTarget.dataset.index
    console.log(index);
    let province = _self.data.data[index].n
    let pid = _self.data.data[index].pid
    let pid1 = _self.data.pid
    console.log(pid);
    console.log(pid1);
    console.log(province);
    if(pid==0){
      console.log(pid);
      console.log(pid1);
      _self.setData({
        province,//省
        pid:1
      })
    }else if((pid!==pid1)&&pid1==1){
      console.log(pid);
      console.log(pid1);
      _self.setData({
        province1:province,//市
        pid:2
      })
    }else if((pid!==pid1)&&pid1==2){
      console.log(pid);
      console.log(pid1);
      _self.setData({
        province2:province,//区
        city_id:pid
      })
    }
    let city = _self.data.data[index]
    if(city.c.length!==0){
      let county = city.c
      console.log(county);
      _self.setData({
        data:county,
      })
    }else{
      let region = _self.data.province+_self.data.province1+_self.data.province2
      console.log(region);
      _self.setData({
        region
      })
      _self.popUp()
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})