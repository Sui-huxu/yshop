// pages/more/address/address.js
import {
  addressListApi,
  addressDefaultApi,
  addressDelApi
} from '../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    addressList: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.getList()
  },
  getList() {
    addressListApi({
      page: 1,
      limit: 20
    }).then(res => {
      console.log(res);
      this.setData({
        addressList: res.data
      })
    })
  },
  checkboxChange(e) {
    let id = e.currentTarget.dataset.id
    addressDefaultApi({
      id
    })
    this.getList()
  },
  delAdress(e) {
    let id = e.currentTarget.dataset.id
    addressDelApi({
      id
    })
    this.getList()
  },
  toEditPage(e) {
    let item = e.currentTarget.dataset.info
    console.log(item);
    wx.navigateTo({
      url: '/pages/more/address/editAddress/editAddress?item=' + JSON.stringify(item),
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getList()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})