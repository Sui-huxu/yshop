// pages/more/myfoot/myfoot.js
import{collectApi,delApi}from'../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    number: {
      page: 1,
      limit: 20,
      type:'foot'
    }, //接口传参
    list:[],//收藏商品
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _self = this
    collectApi(_self.data.number).then(res=>{
      const list = res.data
      this.setData({
        list
      })
    })
  },
  //删除足迹
  delete(e){
    let _seif = this
    console.log(e.currentTarget.dataset.id);
    let id = e.currentTarget.dataset.id
    let sdff={ id, category:"foot"}//删除足迹
    delApi(sdff).then(res=>{
      if(res.status==200){
        collectApi(this.data.number).then(res => {
          const list = res.data
          _seif.setData({
            list
          })  
        })
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})