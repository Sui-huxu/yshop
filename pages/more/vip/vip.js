// pages/more/vip/vip.js
import{gradeApi,taskApi,hotApi}from'../../../http/api'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    information:[],
    message:[],
    length:'',
    reachCount:'',
    current:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    gradeApi().then(res=>{
      let _self = this
      console.log(res);
      let data = res.data
      let task = data.task
      let length =task.list.length
      let reachCount = task.reachCount
      let list = data.list
      console.log(list);
      _self.setData({
        information:list,
        length,
        reachCount
      })
    })
    hotApi().then(res=>{
      let data = res.data
      console.log(data[0].id);
      this.setData({
        message:data
      })
      
    })
  },
  swiper(e){
    let _self = this;
    let {current} = e.detail;
    _self.setData({
      current,
    })
  },
  prodetail(e){
    console.log(e);
    let id = e.currentTarget.dataset.id
    console.log(id);
    wx.navigateTo({
      url: '../../productDetails/productDetails?id='+id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})