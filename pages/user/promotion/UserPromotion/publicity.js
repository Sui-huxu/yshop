// pages/mine/publicity/publicity.js
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },
 //我的推广
 promotion(){
   wx.navigateTo({
     url: '../Poster/pub3',
   })
 },
 //推广人数统计
 statistics(){
  wx.navigateTo({
    url: '../PromoterList/pub2',
  })
 },
 
 //佣金明细
 commission(){
  wx.navigateTo({
    url: '../CommissionDetails/pub',
  })
 },
 //佣金明细
 indent(){
  wx.navigateTo({
    url: '../PromoterOrder/pub1',
  })
 },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})