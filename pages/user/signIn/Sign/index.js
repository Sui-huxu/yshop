// pages/user/signIn/Sign/index.js
import { integralApi ,SigninApi,integralconfigApi,integrallistApi} from "../../../../http/api"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user:{sign: 1},//用户传参
    number:{
      page:1,
      limit:3
    },//签到传参
    userList:[],//用户信息
    data:[],//七天签到
    status:0,//当前签到天数
    sign:{},//积分数据
    isshow:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 头部用户信息接口
    integralApi(this.data.user).then(res=>{
      var userList = res.data
      var status = res.data.signNum * 10
      this.setData({
        userList,
        status
      })
    })
    // 签到日期接口
    integralconfigApi().then(res=>{
      var date = res.data
      console.log(date);
      this.setData({
        date
      })
    })
    // 积分接口
    integrallistApi().then(res=>{
      var data = res.data
      console.log(data);
      var sign =[]
      if(data.length>=3){
       for(let i = 0 ;i<3 ;i++){
         sign.push(data[i])
       }
      }else {
        sign=data
      }
      setTimeout(()=>{
        this.setData({
          sign
        })
      },200)
    })

  },
  // 跳转积分详情
  record(){
    wx.navigateTo({
      url:"../signRecord/index"
    })
  },
  //签到
  signStatus(e){
    console.log(e);
    if(!e.currentTarget.dataset.resd){
      SigninApi().then(res=>{
        // 头部用户信息接口
        integralApi(this.data.user).then(res=>{
          var userList = res.data
          var status = res.data.signNum * 10
          this.setData({
            userList,
            status
          })
        })
        // 签到日期接口
        integralconfigApi().then(res=>{
          var date = res.data
          this.setData({
            date
          })
        })
        // 积分接口
    integrallistApi().then(res=>{
      var data = res.data
      var sign =[]
       for(let i = 0 ;i<3 ;i++){
         sign.push(data[i])
       }
      setTimeout(()=>{
        this.setData({
          sign
        })
      },200)
    })
        let isshow = true;
        console.log(isshow);
        this.setData({
          isshow
        })
      })
    } else {
      wx.showToast({
        title: "已签到",
        icon: 'success',
        duration: 2000
      })
    }
  },
  // 隐藏按钮 已签到
  hidebox(){
    let _self =this;
    let isshow = false;
    _self.setData({
      isshow,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})