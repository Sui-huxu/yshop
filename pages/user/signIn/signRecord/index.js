// pages/user/signIn/signRecord/index.js
import { monthApi} from "../../../../http/api"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    number:{
      page:1,
      limit:3
    },//签到传参
    Arr:[],//积分信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 头部用户信息接口
    monthApi(this.data.number).then(res=>{
      console.log(res);
      let data = res.data
      let list = data.list
      let arr =list[0]
      let Arr = arr.list
      console.log(Arr);
      this.setData({
        Arr
      })
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})