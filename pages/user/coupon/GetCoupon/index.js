// pages/user/coupon/GetCoupon/index.js
import {
  couponsApi,
  receiveApi
} from '../../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    number:{
      page:1,
      limit:8
    },//接口传参
    list:[],//优惠劵
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    couponsApi(this.data.number).then(res=>{
      console.log(res);
      var list = res.data
      this.setData({
        list
      })
    })
  },
  //领取优惠劵e
  draw(e){
    let _seif = this
    let id = e.currentTarget.dataset.couponid
    console.log(id);
    let ree = {couponId:id}
    receiveApi(ree).then(res1=>{
      console.log(res1);
      if(res1.status == 200){
        wx.showToast({
          title: '领取成功',
          icon: 'success',
          duration: 1500//持续的时间
        })
        couponsApi(_seif.data.number).then(res=>{
          console.log(res);
          var list = res.data
          _seif.setData({
            list
          })
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})