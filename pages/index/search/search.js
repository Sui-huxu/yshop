// pages/index/search/search.js
import {
  searchApi,
  productsApi
} from '../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    data1:[],//热门搜索
    dsfb:'',//搜索的商品
    list:{
      page:1,
      limit: 8,
      keyword:'',
      isIntegral:0
    },//搜索内容双向绑定
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    searchApi().then(res => {
      var data1 = res.data
      this.setData({
        data1,
      })
    });
  },
  input(e){
    let name = e.detail.value;
    console.log(name);
    this.setData({
      dsfb:name
    })
  },
  // 搜索商品
  searchGoods(e){
    let _self = this
    let keyword = _self.data.list.keyword
    keyword = _self.data.dsfb
    productsApi(_self.data.list).then(res => {
      console.log(res);
      if(keyword){
        wx.navigateTo({
          url: '../../shop/GoodsList/index?s='+keyword,
        })
      }
    });
  },

  // 热门搜索
  Hot_box(e){
    let el = e.currentTarget.dataset.name
    console.log(el);
    wx.navigateTo({
      url: '../../shop/GoodsList/index?s='+el,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})