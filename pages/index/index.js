// index.js
// 获取应用实例
import { indexApi } from '../../http/api'

Page({
  data: {
    menus:[],//金刚区
    roll0:'',//通知文字1
    roll1:'',//通知文字2
    likeInfo:[],//热门商品
    combinationList:[],//超值拼团
    firstList:[],//商品
    banner:[],
  },
  onLoad() {
    // 文字轮播
    indexApi().then(res => {
      console.log(res);
      let roll0 = res.data.roll[0].info
      let roll1 = res.data.roll[1].title
      let menus = res.data.menus
      let likeInfo = res.data.likeInfo
      let combinationList = res.data.combinationList
      let firstList = res.data.firstList
      let banner = res.data.banner
      this.setData({
        banner,
        menus,
        roll0,
        roll1,
        likeInfo,
        combinationList,
        firstList
      })
    })
  },
  // 跳转搜索页面
  search(){ 
    wx.navigateTo({
      url: '/pages/index/search/search',
    })
  },
  // 跳转金刚区页面
  redirect(url){ 
    let id = url.currentTarget.dataset.url
    console.log(id);
    wx.navigateTo({
    url: id,
    });
  },
  // 跳转拼团商品
  goodsDetails(e){
    let id = e.currentTarget.dataset.id
    console.log(id);
    wx.navigateTo({
      url: '../activity/GroupDetails/index?id='+id,
    })
  },
  getUserProfile() {
  },
  getUserInfo() {
  },
  //跳转拼团页面
  hotList(){
    wx.navigateTo({
      url: '../../pages/activity/GoodsGroup/index'
    })
  },
  // 跳转优惠劵页面
  coupon(){
    wx.navigateTo({
      url: '../../pages/user/coupon/GetCoupon/index'
    })
  },
  // 跳转购物页面
  goodsres(){
    wx.navigateTo({
      url: '../../pages/shop/GoodsList/index',
    })
  },
  // 跳转积分购物页面
  sign(){
    wx.navigateTo({
      url: '/pages/shop/GoodsList/index?id='+1,
    })
  },
  // 跳转详情页
  prodetail(e){
    let id = e.currentTarget.dataset.id
    console.log(id);
    wx.navigateTo({
      url: '/pages/productDetails/productDetails?id='+id,
    })
  }
})