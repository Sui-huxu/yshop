import {
  registerApi,
  mobileApi,
  pwdLoginApi,
  registerresApi
} from '../../http/api'
//在page页面引入app，同时声明变量，获得所需要的全局变量

// pages/login/login.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    loginWay: true,
    pageState: true,
    phoneNumber: '',
    loginCode: '',
    password: '',
    reCode: '',
    rePassword: ''
  },
  changePhoLogin() {
    this.setData({
      loginWay: true
    })
  },
  changePsdLogin() {
    this.setData({
      loginWay: false
    })
  },
  changePageState() {
    let pageState = !this.data.pageState
    this.setData({
      pageState
    })
  },
  getCode() {
    registerApi({
      phone: this.data.phoneNumber,
      type: 'login'
    }).then((res) => {
      console.log(res);
    }).catch(e => {
      console.log(e);
      wx.showToast({
        title: e.msg,
        icon: 'none',
        duration: 2000
      })
    })
  },
  phoneLogin() {
    mobileApi({
      account: this.data.phoneNumber,
      captcha: this.data.loginCode
    }).then(res => {
      console.log(res);
      wx.setStorageSync('token', res.data.token)
      wx.navigateBack()
    })
  },
  pwdLogin() {
    if (this.data.phoneNumber.trim().length < 11) {
      wx.showToast({
        title: '请输入正确的手机号',
        icon: 'none'
      })

    } else {
      if (this.data.password.trim().length < 6) {
        wx.showToast({
          title: '密码需大于六位',
          icon: 'none'
        })
      } else {
        let params = {
          username: this.data.phoneNumber,
          password: this.data.password
        }
        let res = pwdLoginApi(params)
        console.log(res);
      }
    }
  },
  getrecode() {
    if (this.data.phoneNumber.trim().length < 11) {
      wx.showToast({
        title: '请输入正确的手机号',
        icon: 'none'
      })
    } else {
      let params = {
        phone: this.data.phoneNumber,
        type: 'register'
      }
      registerApi(params).catch(e=>{
        wx.showToast({
          title: e.msg,
          icon: 'none',
          duration: 2000
        })
      })
    }
  },
  getregister() {
    if (this.data.reCode.length !== 6) {
      wx.showToast({
        title: '请输入正确的验证码',
        icon: 'none'
      })
    } else {
      if (this.data.rePassword.length < 6) {
        wx.showToast({
          title: '密码的长度需大于6',
          type: 'none'
        })
      } else {
        let params = {
          account: this.data.phoneNumber,
          captcha: this.data.reCode,
          inviteCode: '',
          password: this.data.rePassword,
          spread: ''
        }
        registerresApi(params).then(res => {
          wx.showToast({
            title: '注册成功',
            icon: 'none'
          })
        })
      }
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})