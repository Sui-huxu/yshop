// pages/shop/GoodsList/index.js
import {
  hotApi,
  productsApi,
  productsIntegralApi
} from '../../../http/api'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    imgimg:true,
    id:'',
    s:'',
    number:{
      page:1,
      limit:8,
      keyword:'',
      sid:0,
      priceOrder:'',
      isIntegral: 0
    },//接口传参
    descri:{},//商品详情
    message:{},//推荐
    title:'',//跳转名字
    dsfb:'',
    list:['默认','价格','销量','新品'],
  },


  // 调接口
  products(e){
    let _self = this
    productsApi(_self.data.number).then(res=>{
      console.log(res);
      var descri = res.data
      _self.setData({
        descri,
      })
    })
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _self = this
    let s = options.s
    console.log(s);
    let id = options.id
    let title = options.title
    let goodsId = options.goodsId
    _self.data.title = title
    console.log(id);
    if(id == 1){
      _self.data.number.isIntegral = id
      productsIntegralApi(_self.data.number).then(res=>{
        console.log(res);
        var descri = res.data
        _self.setData({
          descri,
          id
        })
      })
    } else if(goodsId){
      _self.data.number.sid =  Number(goodsId);
      _self.products()
    }else if(s){
      _self.data.number.keyword = s
      _self.products()
    }else {
      _self.products()
    }
    _self.setData({
      title,
      id,
      s
    })
    hotApi().then(res=>{
      let data = res.data
      this.setData({
        message:data
      })
    })
  },

  // 跳转详情页
  prodetail(e){
    let id = e.currentTarget.dataset.id
    console.log(id);
    wx.navigateTo({
      url: '../../../pages/productDetails/productDetails?id='+id,
    })
  },
  
  input(e){
    let name = e.detail.value;
    console.log(name);
    this.setData({
      dsfb:name
    })
  },
  // 搜索商品
  searchGoods(){
    let _self = this
    let keyword = _self.data.number.keyword
    keyword = _self.data.dsfb
    productsApi(_self.data.list).then(res => {
      console.log(res);
      if(keyword){
        wx.navigateTo({
          url: '/pages/shop/GoodsList/index?s='+keyword,
        })
      }
    });
  },

  textRed(priceOrder){
    let _self = this
    let s = _self.data.s
    let id = _self.data.id
    _self.data.number.priceOrder =  priceOrder 
    if(id == 1){
      productsIntegralApi(_self.data.number).then(res=>{
        console.log(res);
        var descri = res.data
        _self.setData({
          descri
        })
      })
    }else{ 
      if(_self.data.id){_self.data.number.sid = _self.data.id}
      if(_self.data.s){_self.data.number.keyword= _self.data.s}
      _self.products()
    }
  _self.setData({
    s,
  })
  },
  
  // 默认的点击事件
  default(e){
    let _self=this
    let {index}=e.currentTarget.dataset
    let {acIndex}=_self.data
    let {priceClick}=_self.data
    let {salesClick}=_self.data
    let {products}=_self.data
    priceClick=false
    salesClick=false
    acIndex=index
    _self.textRed()
    _self.setData({
      acIndex,priceClick,salesClick,products
    })
  },
  // 价格的点击事件
  price(e){
    let _self=this
    let {index}=e.currentTarget.dataset
    let {acIndex}=_self.data
    let {priceUp}=_self.data
    let {priceClick}=_self.data
    let {salesClick}=_self.data
    let {products}=_self.data
    if(!priceClick){
      priceClick=true
    }
    if(priceUp==0){
      priceUp=priceUp+1
    _self.textRed('asc')
    }else{
      priceUp=0
      _self.textRed('desc')
    }
    salesClick=false
    acIndex=index
    _self.setData({
      acIndex,priceUp,priceClick,salesClick,
    })
  },
  // 销量的点击事件
  sales(e){
    let _self=this
    let {index}=e.currentTarget.dataset
    let {acIndex}=_self.data
    let {salesClick}=_self.data
    let {salesUp}=_self.data
    let {priceClick}=_self.data
    let {products}=_self.data
    if(!salesClick){
      salesClick=true
    }
    if(salesUp==0){
      salesUp=salesUp+1
      _self.textRed('asc')
    }else{
      salesUp=0
      _self.textRed('desc')
    }
    priceClick=false
    acIndex=index
    _self.setData({
      acIndex,salesUp,salesClick,priceClick
    })
  },
  // 新品的点击事件
  news(e){
    let _self=this
    let {index}=e.currentTarget.dataset
    let {acIndex}=_self.data
    let {priceClick}=_self.data
    let {salesClick}=_self.data
    let {products}=_self.data
    priceClick=false
    salesClick=false
    acIndex=index
    if(priceClick){
      _self.textRed('asc')
    }else{
      _self.textRed('desc')
    }
    _self.setData({
      acIndex,priceClick,salesClick
    })
  },



  // 改变图标
  moreIme(){
    console.log(1);
    let _self=this
    let imgimg = !_self.data.imgimg
    _self.setData({
      imgimg
    })
  },

  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})