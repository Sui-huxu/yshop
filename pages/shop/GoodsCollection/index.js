// pages/shop/GoodsCollection/index.js
import {
  collectApi,
  delApi,
  hotApi
} from '../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    number: {
      page: 1,
      limit: 20,
      type:'collect'
    }, //接口传参
    list:[],//收藏商品
    userbalance:{},
    message:[],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    collectApi(this.data.number).then(res => {
      const list = res.data
      this.setData({
        list
      })
    })
    
    hotApi().then(res=>{
      let data = res.data
      console.log(data[0].id);
      this.setData({
        message:data
      })
    })
  },

  //删除收藏
  delete(e){
    let _seif = this
    console.log(e.currentTarget.dataset.id);
    let id = e.currentTarget.dataset.id
    let sdff={ id, category:"collect"}//删除收藏
    delApi(sdff).then(res=>{
      if(res.status==200){
        collectApi(this.data.number).then(res => {
          const list = res.data
          this.setData({
            list
          })  
        })
      }
    })
  },
  prodetail(e){
    console.log(e);
    let id = e.currentTarget.dataset.id
    console.log(id);
    wx.navigateTo({
      url: '../../productDetails/productDetails?id='+id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {


  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})