// pages/category/category.js

import { listApi } from "../../http/api";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    msg:[],
    children:[],
    datas:[],
    index:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // https://wxapi.yixiang.co/api/category
    this.getCategoryList()
  },

  // 跳转搜索页面
  search(){ 
    wx.navigateTo({
      url: '/pages/index/search/search',
    })
  },
  // 获取分类
  getCategoryList(){
    listApi()
    .then(result=>{
      console.log(result);
      let message = result
      let {data} = message
      console.log(data);
      let datas = data
      console.log(datas);
      let msg = datas
      let i = this.data.index;
      console.log(msg);
      let {children} = msg[i];
      console.log(msg);
      // console.log(data);
      // console.log(datas);
      console.log(children);
      this.setData({
        msg,
        children,
        datas
      })
    })
  },
  /**
     * 左侧菜单点击事件
     */
    chooseMenu(e){
      console.log(e.currentTarget.dataset);
      let {index} = e.currentTarget.dataset
      console.log(index);
      let msg = this.data.msg;
      let {children} = msg[index];
      this.setData({
        children,
        index
      })
    },
    //跳转详情
    toList(e){
      let id = e.currentTarget.dataset.id;
      let name = e.currentTarget.dataset.name;
      wx.navigateTo({
        url: '/pages/shop/GoodsList/index?goodsId='+id+'&title='+name,
      })
    },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})