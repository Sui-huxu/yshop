// pages/mine/mine.js
// 获取应用实例
import {
  userApi,
  infoApi
} from '../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    message: [],
    loginState: false,
    login: true,
    information: {}, //用户信息
    orderStatusNum:{}//订单信息
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    if (wx.getStorage('token')) {
      this.getUserApi()
      userApi().then(res => {
        console.log(res);
        var data = res.data;
        console.log(data);
        var menus = data.routine_my_menus;
        console.log(menus);
        this.setData({
          message: menus
        })
      })
    }
  },
  getUserApi(){
    infoApi(this.data.login).then(res => {
      console.log(res);
      let data = res.data
      let orderStatusNum = data.orderStatusNum
      this.setData({
        information: data,
        orderStatusNum
      })
    })
  },
  balance_click() {
    wx.navigateTo({
      url: './balance/balance',
    })
  },
  integral_click() {
    wx.navigateTo({
      url: './integral/integral',
    })
  },
  coupon_click() {
    wx.navigateTo({
      url: './coupon/coupon',
    })
  },
  toLoginPage() {
    wx.navigateTo({
      url: '/pages/login/login',
    })
  },
  // 订单跳转
  toorder() {
    wx.navigateTo({
      url: '/pages/mine/order/order',
    })
  },
  TOorder(e){
    console.log(e);
    let active = e.currentTarget.dataset.active
    if (active == 4) {
      wx.navigateTo({
        url: '/pages/mine/order/ReturnList/index',
      })
    } else {
      wx.navigateTo({
        url: '/pages/mine/order/order?active='+active,
      })
    }
    
  },
  tomore(e) {
    console.log(e);
    let name = e.currentTarget.dataset.name
    console.log(name);
    switch (true) {
      case name == '我的足迹':
        wx.navigateTo({
          url: '../more/myfoot/myfoot',
        })
        break;
      case name == '会员中心':
        wx.navigateTo({
          url: '../more/vip/vip',
        })
        break;
      case name == '优惠券':
        wx.navigateTo({
          url: '../mine/coupon/coupon',
        })
        break;
      case name == '收藏商品':
        wx.navigateTo({
          url: '../shop/GoodsCollection/index',
        })
        break;
      case name == '地址管理':
        wx.navigateTo({
          url: '../more/address/address',
        })
        break;
      case name == '我的推广':
        wx.navigateTo({
          url: '/pages/user/promotion/UserPromotion/publicity',
        })
        break;
      case name == '我的余额':
        wx.navigateTo({
          url: '../mine/balance/balance',
        })
        break;
      case name == '我的积分':
        wx.navigateTo({
          url: '../mine/integral/integral',
        })
        break;
      case name == '商户管理':
        wx.showToast({
          title: '您还不是管理员！！',
          icon: 'none',
          duration: 1500 //持续的时间
        })
        break;
      case name == '砍价记录':
        wx.navigateTo({
          url: '../more/bargain/bargain',
        })
        break;
      default:
        wx.showToast({
          title: '您没有核销权限，请后台店员设置！！',
          icon: 'none',
          duration: 1500 //持续的时间
        })
        break;
    }
  },
  toSetPage() {
    wx.navigateTo({
      url: '/pages/mine/sitting/sitting',
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getUserApi()
    
    if (wx.getStorageSync('token')) {
      this.setData({
        loginState: true
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})