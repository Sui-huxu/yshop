// pages/mine/balance/balance.js
import {hotApi} from '../../../http/api'
import {balanceApi} from '../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    message:[],
    tabIndex:0,
    userbalance:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    hotApi().then(res=>{
      
      let data = res.data
      console.log(data);
      console.log(data[0].id);
      this.setData({
        message:data
      })
    })
    
    balanceApi().then(res=>{
      console.log(res);
      let data = res.data
      this.setData({
        userbalance:data
      })
    })
  },
  click(e){
    console.log(e);
    let tabIndex = e.currentTarget.dataset.tabindex
    console.log(tabIndex);
    wx.navigateTo({
      url: './record/record?tabIndex='+tabIndex,
    })
  },
  prodetail(e){
    console.log(e);
    let id = e.currentTarget.dataset.id
    console.log(id);
    wx.navigateTo({
      url: '../../productDetails/productDetails?id='+id,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})