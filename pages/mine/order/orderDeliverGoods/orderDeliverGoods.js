import {unpayApi,cancelorderApi,integralInfo,wechatpayApi} from '../../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    res: {},
    status: '',
    arr: [],
    nowMoney: '',
    hideGood: true,
    animationGood: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _self = this;
    let {id,status} = options;
    let arr = [     
      {
        id: 0,
        text: '待付款',
      },
      {
        id: 1,
        text: '待发货',
      },
      {
        id: 2,
        text: '待收货',
      },
      {
        id: 3,
        text: '待评价',
      },
      {
        id: 4,
        text: '已完成',
      }]
    unpayApi(id).then(res=>{
      _self.setData({
        res: res.data,
        status: status,
        arr: arr,
      })
    })
    integralInfo().then(res=>{
      _self.setData({
        nowMoney: res.data.nowMoney
      })
    })
  },
  // 取消订单
  cancle(){
    let _self = this;
    wx.showModal({
      title: '确认取消该订单？',
      content: '',
      complete: (res) => {
        if (res.cancel) {
          // 不取消订单
        }
        if (res.confirm) {
          // 确认取消该订单
          cancelorderApi({
            id: _self.data.res.orderId
          }).then(res=>{
            console.log(res);
          })
        }
        wx.navigateTo({
          url: '/pages/Order-Information/Order-Information?index='+_self.data.status,
        })
      }
    })
  },
  // 弹窗显示
  seeGood: function () {
    var that = this;
      that.setData({
        hideGood: false
      })
      // 创建动画实例
      var animation = wx.createAnimation({
        duration: 400,//动画的持续时间
        timingFunction: 'ease',//动画的效果 默认值是linear->匀速，ease->动画以低速开始，然后加快，在结束前变慢
      })
      that.animation = animation; //将animation变量赋值给当前动画
      var time1 = setTimeout(function () {
        //调用动画--滑入
        that.animation.translateY(-275).step() // 在y轴偏移，然后用step()完成一个动画
        that.setData({
          //动画实例的export方法导出动画数据传递给组件的animation属性
          animationGood: that.animation.export()
        })
        clearTimeout(time1);
        time1 = null;
      }, 100)
  },
  // 弹窗隐藏
	hideGood:function () {
    var that = this;
    var animation = wx.createAnimation({
      duration: 400,//动画的持续时间 默认400ms
      timingFunction: 'ease',//动画的效果 默认值是linear
    })
    that.animation = animation
    //调用动画--滑出
    that.animation.translateY(0).step()
    that.setData({
      animationGood: that.animation.export(),
    })
    var time1 = setTimeout(function () {
      that.setData({
        hideGood: true
      })
      clearTimeout(time1);
      time1 = null;
    }, 220)//先执行下滑动画，再隐藏模块
  },
  // 微信支付
  weixinpay(){
    let _self = this;
    wx.showLoading({
      title: '支付中',
      mask: true,
    })
    wechatpayApi({
      from: "weixinh5",
      paytype: _self.data.res.payType,
      uni: _self.data.res.orderId
    }).then(res=>{
      console.log(res);
      wx.redirectTo({
        url: `/pages/orderDeliverGoods/orderDeliverGoods?status=${status}&&id=${res.result.orderId}`,
      })
    })
    
  },
  // 余额支付
  yuepay(){
    let _self = this;
    wx.showLoading({
      title: '支付中',
      mask: true,
    })
    wechatpayApi({
      from: "weixinh5",
      paytype: "yue",
      uni: _self.data.res.orderId
    }).then(res=>{
      wx.showToast({
        title: res.payMsg,
        icon: "none"
      })
      wx.redirectTo({
        url: '/pages/mine/order/orderDeliverGoods/orderDeliverGoods?id='+_self.data.res.orderId+'&status=1',
      })
    })
    
  },
  // 申请售后
  aftersales(){
    let _self = this;
    wx.redirectTo({
      url: '/pages/afterSales/afterSales?id='+_self.data.res.orderId,
    })
  },
  toAppraise(e){
    console.log(e);
    let unique = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/mine/order/orderDeliverGoods/toEvaluate/toEvaluate?unique='+unique,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})