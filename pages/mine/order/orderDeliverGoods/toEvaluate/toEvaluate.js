// pages/toEvaluate/toEvaluate.js
import {orderProductApi,orderCommentApi} from "../../../../../http/api"
Page({

  /**
   * 页面的初始数据
   */
  data: {
    score:0,
    scores:0,
    list:[],
    productInfo:[],
    attrInfo:[],
    data:[],
    unique:[],
    value:'',
    productScore:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _self = this
    let unique = options.unique
    _self.setData({
      unique
    })
    orderProductApi(
      {
        unique,
      }
    ).then(res=>{
        console.log(res);
        let {data} = res;
        console.log(data);
        let {productInfo} = data;
        console.log(productInfo);
        let {attrInfo} = productInfo;
        console.log(attrInfo);
        _self.setData({
          data,
          productInfo,
          attrInfo
        })
      }
    )
  },

  shopXing(e){
    let _self = this
    let index = e.currentTarget.dataset.index
    _self.setData({
      score:index+1,
    })
  },
  giveXing(e) {
    let _self = this
    let indexs = e.currentTarget.dataset.indexs
    _self.setData({
      scores:indexs+1
      
    })
  },
  handleChooseImg(e) {
    let _self = this;
    wx.chooseMedia({
      count: 1,
      mediaType: ['image', 'video'],
      sourceType: ['camera', 'album'], //camera调拍照，album是手机相册
      success: (res) => {
        console.log(res);
        let userAvatar = res.tempFiles[0].tempFilePath;
        let list = _self.data.list;
        list.push(userAvatar)
          console.log(list);
          let arr=[]
        for (let i = 0; i < list.length; i++) {
           wx.uploadFile({
            url: 'https://wxapi.yixiang.co/api/api/upload', //仅为示例，非真实的接口地址
            filePath:list[i],
            name: 'file',
            success(res) {
                console.log(res);
                let imageUrl = JSON.parse(res.data).link;
                console.log(imageUrl);
                arr.push(imageUrl )
                console.log(arr);
              _self.setData({
                list,
              })
            }
          })
        }
      }
   });
  },
  deleteImg(e){
    let _self = this;
    let index = e.target.dataset.index;
    console.log(index);
    let imgShow = _self.data.list;
    imgShow.splice(index, 1);
    _self.setData({
      list: imgShow,
    })
  },
  // 立即评价
  appraise(){
    let _self = this;
    let list = _self.data.list;
    let pics = list.join(',');
    let comment = _self.data.value;
    console.log(comment);
    let productScore = _self.data.score
    console.log(productScore);
    let serviceScore = _self.data.scores
    console.log(serviceScore);
    let unique = _self.data.unique
    console.log(unique);
    orderCommentApi(
      {
        comment,
        pics,
        productScore,
        serviceScore,
        unique,
      }
    ).then(res=>{
      console.log(res);
    })
  },
  texts(e){
    console.log(e);
    let _self = this;
    let value = e.detail.value
    console.log(value);
    _self.setData({
      value,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})