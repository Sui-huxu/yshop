// pages/mine/order/order.js
import {
  orderCancelApi,
  orderDataApi,
  orderListApi,
  orderTakeApi
} from '../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    dataList: {},
    active: 0,
    shopList: []

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options);
    if (options.length !== 0) {
      this.setData({
        active: options.active
      })
    }
    this.getData()
  },
  getData() {
    orderDataApi().then(res => {
      console.log(res);
      this.setData({
        dataList: res.data
      })
    })
    orderListApi({
      page: 1,
      limit: 20,
      type: this.data.active ? this.data.active : 0
    }).then(res => {
      console.log(res);
      this.setData({
        shopList: res.data
      })
    })
  },
  changeBar(e) {
    console.log(e);
    let i = e.currentTarget.dataset.index
    this.setData({
      active: i
    })
    orderListApi({
      page: 1,
      limit: 20,
      type: i
    }).then(res => {
      console.log(res);
      this.setData({
        shopList: res.data
      })
    })
  },
  toOrderDetails(e) {
    console.log(e);
    let id = e.currentTarget.dataset.info
    let status = e.currentTarget.dataset.status
    wx.navigateTo({
      url: `/pages/mine/order/orderDeliverGoods/orderDeliverGoods?id=${id}&&status=${status}`
    })
  },
  topay(e) {
    console.log(e);
    let id = e.currentTarget.dataset.id
    wx.navigateTo({
      url: '/pages/mine/order/orderDetails/orderDetails?id=' + id,
    })
  },
  tocancel(e) {
    console.log(e);
    let id = e.currentTarget.dataset.id
    orderCancelApi({
      id
    }).then(res => {
      console.log(res);
      this.getData()
    })
  },
  toOrderTake(e){
    console.log(e);
    let uni = e.currentTarget.dataset.id
    orderTakeApi({uni}).then(res=>{
      console.log(res);
      this.getData()
    })
  },
  tologisitics(){
    wx.navigateTo({
      url: '../order/orderDeliverGoods/logistics/logistics',
    })
  },
  totalGoodsNum(i) {
    let num = 0
    i.map(item=>{
      item.cartNum += num
    })
    return num
  },
  totalGoodsPrice(i){
    let price = 0
    i.map(item=>{
      item.price += price
    })
    return price
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.getData()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})