// pages/order/OrderReturnDetail/index.js
import {storeDetailApi, revokeApi} from "../../../../../http/api";
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let {id, key} = options;
    let _self = this;
    _self.getData(id, key);
  },
  // 获取数据
  getData(id, key) {
    let _self = this;
    storeDetailApi(id, key).then(res => {
      let storeList = res.data;
      _self.setData({
        storeList,
      })
    })
  },
  // 点击撤销申请
  handlerevoke(e) {
    let {id, key} = e.currentTarget.dataset;

    wx.showModal({
      title: '提示',
      content: '确定要撤销申请吗？',
      complete: (res) => {
        if (res.confirm) {
          revokeApi(id, key).then(res => {
            let msg = res.msg;
            wx.showToast({
              icon: 'success',
              title: msg,
            })
          })
        }
      }
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})