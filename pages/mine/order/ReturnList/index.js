// pages/order/ReturnList/index.js
import {storeAfterSalesListApi} from "../../../../http/api";
Page({

  /**
   * 页面的初始数据
   */
  data: {
    selected: true,
    selected1: false,
    selected2: false,
    type: 0, // 默认进入全部
    productsList: [], // 退款订单数据
  },
  // 接口要用到的参数
  Params: {
    page: 1,
    limit: 5,
    type: 0
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _self = this;
    _self.getData();
  },
  // 获取退款商品数据
  getData() {
    let _self = this;
    storeAfterSalesListApi({..._self.Params}).then(res => {
      let productsList = res.data;
      _self.setData({
        productsList: [..._self.data.productsList, ...productsList]
      })
    })
  },
  // 点击切换到全部
  selected: function () {
    let _self = this;
    _self.setData({
      selected: true,
      selected1: false,
      selected2: false,
    });
    _self.data.productsList = [];
    _self.Params.page = 1;
    _self.Params.type = 0;
    _self.getData();
  },
  // 点击切换到售后中
  selected1: function () {
    let _self = this;
    _self.setData({
      selected: false,
      selected1: true,
      selected2: false,
    });
    _self.data.productsList = [];
    _self.Params.page = 1;
    _self.Params.type = 1;
    _self.getData();
  },
  // 点击切换到已完成
  selected2: function () {
    let _self = this;
    _self.setData({
      selected: false,
      selected1: false,
      selected2: true,
    });
    _self.data.productsList = [];
    _self.Params.page = 1;
    _self.Params.type = 2;
    _self.getData();
  },
  // 点击跳转到售后详情页
  handleGoStoreDetail(e) {
    let {id} = e.currentTarget.dataset;
    let {key} = e.currentTarget.dataset;
    wx.navigateTo({
      url: `./OrderReturnDetail/index?key=${key}&id=${id}`,
    })
  },
  // 点击跳转到商品详情页
  handleGoProductDetail(e) {
    let {id} = e.currentTarget.dataset;
    console.log(id);
    wx.navigateTo({
      url: `../../Product-details/Product-details?id=${id}`,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    let _self = this;
    _self.Params.page++;
    _self.getData();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})