// pages/mine/sitting/sitPassword/sitPassword.js
import {
  registerApi,
  registerResetApi
} from '../../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    newPas: '',
    newPasCon: '',
    phone: '',
    code:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let phone = options.phone
    console.log(phone);
    this.setData({
      phone
    })
  },
  getCode() {
    if (this.data.newPas !== this.data.newPasCon) {
      wx.showToast({
        title: '两次输入密码不一致',
      })

    } else {
      if (this.data.newPasCon.length <= 6) {
        wx.showToast({
          title: '密码长度长于6',
        })
      } else {
        registerApi({
          phone: this.data.phone
        })
      }
    }

  },
  conResat() {
    let params = {
      account:this.data.phone,
      captcha:this.data.code,
      password:this.data.newPasCon
    }
    registerResetApi(params).then(res=>{
      wx.navigateTo({
        url: '/pages/login/login',
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})