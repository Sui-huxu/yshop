// pages/mine/sitting/sitting.js
import {
  infoApi,
  editApi,
  logoutApi
} from '../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: [],
    imageUrl: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    infoApi({
      login: true
    }).then(res => {
      console.log(res);
      this.setData({
        userInfo: res.data
      })
    })
  },
  toSitPassword() {
    let phone = this.data.userInfo.phone
    wx.navigateTo({
      url: '/pages/mine/sitting/sitPassword/sitPassword?phone='+ phone
    })
  },
  pictureClick() {
    let _self = this
    wx.chooseMedia({
      count: 1,
      mediaType: ['image', 'video'],
      sourceType: ['camera', 'album'], //camera调拍照，album是手机相册
      success: (res) => {
        console.log(res);
        let imgs = res.tempFiles[0].tempFilePath
        wx.uploadFile({
          url: 'https://wxapi.yixiang.co/api/api/upload', //仅为示例，非真实的接口地址
          filePath: imgs,
          name: 'file',
          success(res) {
            let imageUrl = JSON.parse(res.data).link
            _self.setData({
              imageUrl
            })
          }
        })
      }
    });
  },
  modify() {
    let _self = this;
    let userAvatar = _self.data.imageUrl;
    let userName = _self.data.userInfo.nickname;
    console.log(userAvatar, userName);
    editApi({
      avatar: userAvatar,
      nickname: userName
    }).then(res => {
      console.log(res);
      wx.switchTab({
        url: '/pages/mine/mine',
      })
    })
  },
  logOut() {
    console.log(wx.getStorageSync('token'));
    wx.showModal({
      title: '提示',
      content: '确认退出登录？',
      success(res) {
        if (res.confirm) {
          logoutApi().then(res => {
            if (res.status == 200) {
              wx.setStorageSync('token', '');
              console.log(wx.getStorageSync('token'));
              wx.navigateTo({
                url: '../../login/login',
              })
            } else {
              wx.showToast({
                title: '退出登录失败',
              })
            }
          })
        } else if (res.cancel) {

        }
      }
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})