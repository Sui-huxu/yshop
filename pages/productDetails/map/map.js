// pages/map/map.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    key: "CENBZ-VO6W3-ACP3Y-3N5JY-RPQZJ-UOBWN",
    markers: [],
    latitude:'',
    longitude:'',

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _self = this
    let info = wx.getStorageSync('message')
    console.log(info);
    let image = info.image
    let latitude = info.latitude
    let longitude = info.longitude
    let id = info.id
    let markers = [{id: id,
                    latitude: latitude,
                    longitude: longitude,
                    iconPath: image,
                    width:50,
                    height:50,
                  }]
    _self.setData({
      latitude,
      longitude,
      markers,
    })
    // let {latitude,image} = options;
    // latitude = latitude.split(',');
    // console.log(latitude);
    // this.setData({
    //   latitude: latitude[0],
    //   longitude: latitude[1],
    //   markers: [{
    //     id: 1,
    //     latitude: "32.11683",
    //     longitude: "114.05857",
    //     iconPath: image,
    //   }],
    // })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})