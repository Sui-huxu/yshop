// pages/orderDetails/orderDetails.js
import { getbalance,integral,orderCoomputed ,addressListApi,couponsOrder,orderPaymentsApi} from "../../../http/api";
Page({
  /**
   * 页面的初始数据
   */
  data: {
    mark:'',
    selected: true,
    selected1: false,
    selected2: false,
    selected3: false,
    id:"",
    cartInfo:[],
    data:[],
    integral:[],
    addressId:[],
    checked:'1',
    isShow: 'hidden',
    isHidden: 'hidden',
    location:[],
    priceGroup:{},//价格
    on:false,//选择
    foo:false,
    couponArray:[],//优惠劵数据
    num:0,//支付方式
    cartId:'',//商品编码
    addressInfo:{},//地址
    systemStore:{},
    phone:'',
    linkman:'',
    payPricefasdsd:0,
    usableCoupon:{},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _self = this;
    console.log(options);
    let cartId = options.cartId
    console.log(cartId);
    couponsOrder('/'+cartId).then(res=>{
      let couponArray = res.data
      console.log(couponArray);
      _self.setData({couponArray})
    })
    getbalance({
      cartId
    }).then(result=>{
      console.log(result);
      let msg = result;
      let {data} = msg;
      console.log(data);
      let priceGroup = result.data.priceGroup
      console.log(priceGroup);
      let payPricefasdsd = Number(priceGroup.totalPrice) + Number(priceGroup.storePostage) 
      console.log(payPricefasdsd);
      let cartInfo = data.cartInfo;
      let addressInfo = data.addressInfo;
      let systemStore = data.systemStore
      let usableCoupon = data.usableCoupon
      console.log(addressInfo);
      _self.setData({
        usableCoupon,
        payPricefasdsd,
        data,
        priceGroup,
        cartInfo,
        addressInfo,
        systemStore
      })
    })
    // 积分
    integral({
      login: true
    }).then(result=>{
        let {data} = result;
        let integral = data.integral
        _self.setData({
          integral
        })
    })
  },
  //支付方式
  modeOfPayment(e){
    let _self =this
    let num = e.currentTarget.dataset.mode
    _self.setData({num})
  },
  //选择积分
  checkboxChange(e){
    let _self = this
    var checkArr = e.detail.value[0];
    let on = !_self.data.on
    let foo = !_self.data.foo
    console.log(on);
    console.log(foo);
    if(checkArr){
      _self.data.console = '1'
    }else {
      _self.data.console = '0'
    }
    let Coupon = 0
    if(_self.data.usableCoupon){
      Coupon = _self.data.usableCoupon.id
    }
    orderCoomputed({
      addressId:_self.data.data.addressInfo.id,
      couponId:Coupon,
      shipping_type:1,
      useIntegral:_self.data.console
    },'/'+_self.data.data.orderKey).then(res=>{
      console.log(res);
      let priceGroup = res.data.result
      _self.setData({
        priceGroup,
        on,
        foo
      })
    })
  },
  //联系人内容
  phone(e){
    let _self = this
    let phone = e.detail.value
    _self.setData({
      phone
    })
  },
  //联系人内容
  linkman(e){
    let _self = this
    let linkman = e.detail.value
    _self.setData({
      linkman
    })
  },
  //选择优惠劵
  radioChange:function(e){
    var _self = this;
    console.log(e.detail.value);
    let  couponArray = _self.data.couponArray
    for(let i =0;i<couponArray.length ;i++){
      if(couponArray[i].id == e.detail.value){
        let obj = 0
        if(_self.data.foo){
          obj = 1
        }
        orderCoomputed({
          addressId:_self.data.data.addressInfo.id,
          couponId:e.detail.value,
          shipping_type:1,
          useIntegral:obj
        },'/'+_self.data.data.orderKey).then(res=>{
          console.log(res);
          let priceGroup = res.data.result
          _self.setData({
            priceGroup
          })
        })
        _self.setData({
          usableCoupon:couponArray[i]
        })
        _self.off()
      }
    }
  },
  //不使用优惠劵
  nonuse(){
    let _self = this
    _self.setData({
      usableCoupon:{}
    })
    _self.off()
  },
  // 跳完成支付页面
  toOrderPayment(e){
    let _self = this;
    let addressId = _self.data.addressInfo.id //add
    let couponId = 0//优惠劵id
    if(_self.data.usableCoupon){
      couponId = _self.data.usableCoupon.id
    }
    let from = 'weixinh5'
    let mark=_self.data.mark //备注
    let payType = '' //支付方式
    if(_self.data.num == 0){
      payType = 'weixin'
    }else{
      payType = 'yue'
    }
    let phone = ''
    let pinkId=0
    let realName = ''
    let shippingType=1
    let storeId = _self.data.systemStore.id
    let useIntegral = 0
    if(_self.data.on){
      useIntegral = 1
    }
    console.log(addressId ,couponId,from,mark,payType,phone,pinkId,realName,shippingType,storeId,useIntegral,_self.data.data.orderKey);
    if(_self.data.selected){
      orderPaymentsApi({addressId ,couponId,from,mark,payType,phone,pinkId,realName,shippingType,storeId,useIntegral},'/'+_self.data.data.orderKey).then(res=>{
        console.log(res);
        wx.showToast({
          title: res.msg,
          icon:'none',
          duration:2000
        })
        let id = res.data.result.orderId
        console.log(id);
        wx.redirectTo({
          url: '/pages/mine/order/orderDeliverGoods/orderDeliverGoods?id='+id,
        })
      })
    } else {
      console.log(_self.data.linkman);
      var reg_user = /^[\u4e00-\u9fa5]{2,6}$/;  
      if(!reg_user.test(_self.data.linkman)){
        wx.showToast({
          title: '联系人不正确',
        })
        return
      }
      console.log(_self.data.phone);
      var reg_tel = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;
      if(!reg_tel.test(_self.data.phone)){
        wx.showToast({
          title: '联系电话不正确',
        })
        return
      }
      //输入框完成验证
      realName = _self.data.linkman
      phone = _self.data.phone
      shippingType=2
      orderPaymentsApi({addressId ,couponId,from,mark,payType,phone,pinkId,realName,shippingType,storeId,useIntegral},'/'+_self.data.data.orderKey).then(res=>{
        console.log(res);
        wx.showToast({
          title: res.msg,
          icon:'none',
          duration:2000
        })
        let id = res.data.result.orderId
        console.log(id);
        wx.redirectTo({
          url: '/pages/mine/order/orderDeliverGoods/orderDeliverGoods?id='+id,
        })
      })
    }
  },
  selected: function (e) {
    let _self = this
    let on = _self.data.foo
    console.log(on);
    this.setData({
      selected: true,
      selected1: false,
      selected2: false,
      selected3: false,
      on
    })
  },
  selected1: function (e) {
    let _self = this
    let foo = _self.data.on
    console.log(foo);
    this.setData({
      selected: false,
      selected1: true,
      selected2: false,
      selected3: false,
      foo
    })
  },

  //选择地址
  selectAddress(e){
    let _self = this
    let id = e.currentTarget.dataset.id
    let data = {
      addressId: id,
      couponId:0,
      shipping_type:1,
      useIntegral:0,
    }
    orderCoomputed(data,'/'+_self.data.data.orderKey).then(res=>{
      console.log(res);
      let priceGroup = res.data.result
      _self.setData({priceGroup})
      addressListApi().then(ress=>{
        console.log(ress.data.length);
        for(let i=0;i<ress.data.length;i++){
          console.log(ress.data[i]);
          if(ress.data[i].id == id){
            let addressInfo = ress.data[i]
            _self.setData({addressInfo})
            _self.overflow()
          }
        }
      })
    })
  },
  // 添加新地址
  addsite(){
    wx.navigateTo({
      url: '/pages/more/address/editAddress/editAddress',
    })
  },
  //关闭弹窗
  overflow() {
    let _self = this;
    _self.data.isShow = "hidden";
    _self.animation.translateY(800).step();
    _self.setData({
      // export导出动画
      animationData: _self.animation.export(),
      isShow: _self.data.isShow,
    })
  },
  
  //关闭弹窗
  off() {
    let _self = this;
    _self.data.isHidden = "hidden";
    _self.coupon.translateY(800).step();
    _self.setData({
      // export导出动画
      couponData: _self.coupon.export(),
      isHidden: _self.data.isHidden,
    })
  },
  //开启弹窗
  pop_up(e) {
    let _self = this;
    if(e.currentTarget.dataset.i == 0){
      addressListApi().then(res=>{
        console.log(res);
        let location = res.data
        _self.setData({
          location
        })
      })
      _self.data.isShow = "show";
      // 创建动画
      let animation = wx.createAnimation({
        // 时间
        duration: 200,
        // 动画效果
        timingFunction: 'ease-in-out',
      })
      // 给小程序对象添加一个动画属性=创建动画的实例
      _self.animation = animation;
      // 动画怎么动（浅拷贝）
      // y轴上为负的，y轴下为负 
      // step停止
      animation.translateY(-800).step();
      _self.setData({
        // export导出动画
        animationData: _self.animation.export(),
        isShow: _self.data.isShow,
      })
    } else if(e.currentTarget.dataset.i == 1){
      _self.data.isHidden = "show";
      // 创建动画
      let coupon = wx.createAnimation({
        // 时间
        duration: 200,
        // 动画效果
        timingFunction: 'ease-in-out',
      })
      // 给小程序对象添加一个动画属性=创建动画的实例
      _self.coupon = coupon;
      // 动画怎么动（浅拷贝）
      // y轴上为负的，y轴下为负 
      // step停止
      coupon.translateY(-800).step();
      _self.setData({
        // export导出动画
        couponData: _self.coupon.export(),
        isHidden: _self.data.isHidden,
      })
    }else{
      wx.navigateTo({
        url: '/pages/productDetails/addresslist/addresslist',
      })
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }

  
})