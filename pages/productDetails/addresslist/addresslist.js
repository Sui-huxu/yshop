// pages/productDetails/addresslist/addresslist.js
import{storlistApi}from'../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    number:{
      latitude: '',
      longitude: '',
      page: 1,
      limit: 20
    },
    list:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let _self = this
    storlistApi(_self.data.number).then(res=>{
      console.log(res);
      let data = res.data
      let list = data.list
      console.log(list);
      _self.setData({
        list
      })
    })
  },
  tomap(e){
    let _self = this
    console.log(e);
    let index = e.currentTarget.dataset.stor
    wx.setStorageSync('message', _self.data.list[index])
    wx.navigateTo({
      url: '../map/map',
    })
  },
  container(e){
    let _self = this
    let id = e.currentTarget.dataset.store;
    let res = _self.data.list[id]
    let pages = getCurrentPages();
    let prevPage = pages[pages.length - 2];
    prevPage.setData({
      systemStore:res
    })
    wx.navigateBack({
      url: 1,
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})