// pages/productDetails/evaluatelist/evaluatelist.js
import{replyApi,replyListApi}from'../../../http/api'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    message:[],
    info:{},
    goodslist:[],//获取的评论列表
    page:1,//当前页数
    limit:8,//每一页几条数据
    totalpages:9,//数据一共的页数
    id:''
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    let url = '/'+ options.id
    console.log(url);
    let _self = this
    _self.setData({
      id:options.id
    })
    replyListApi({page: 1,limit:8,type: 0},url).then(res=>{
      console.log(res);
      let data =res.data
      _self.setData({
        message:[..._self.data.message,...data]
      })
    })
    replyApi({page: 1,limit:8,type: 0},url).then(res=>{
      let data = res.data
      console.log(data);
      _self.setData({
        info:data
      })
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {
    let _self = this
    let id = _self.data.id
    let page = _self.data.page+1
    let url = '/'+id
    _self.setData({
      page
    })
    replyListApi({page:page,limit:8,type: 0,},url).then(res=>{
      console.log(res);
      let data =res.data
      console.log(data.length);
      _self.setData({
        message:[..._self.data.message,...data]
      })
      if(data.length<_self.data.limit){
        wx.showToast({
          title: '已经到底了',
        })
        return;
      }
    })
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})